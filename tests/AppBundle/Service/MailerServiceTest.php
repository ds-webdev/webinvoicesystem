<?php
/**
 * Created by PhpStorm.
 * User: dsiko
 * Date: 11.03.2018
 * Time: 13:45
 */

namespace Tests\AppBundle\Service;


use AppBundle\Service\MailerService;
use PHPMailer\PHPMailer\PHPMailer;
use PHPUnit\Framework\TestCase;

class MailerServiceTest extends TestCase {

public function testSend()
{
    $mailer = $this->createMock(PHPMailer::class);

    $mailerService = new MailerService($mailer, 'P@ssw0rd');

    $mailer->expects($this->any())
        ->method('send')
        ->willReturn(0, 1);

    $result = $mailerService->send();

    $this->assertTrue(is_bool($result));
}

}