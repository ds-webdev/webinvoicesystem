<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase {

    public function testIndex() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Internetowy system wystawiania faktur', $crawler->filter('#container h1')->text());
    }

public function testContractorAction() {
    $client = static::createClient();

    $crawler = $client->request('GET', '/aplikacja/kontrahenci');
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    $this->assertContains('Kontrahenci', $crawler->filter('.content-app h1.page-header')->text());
}

}
