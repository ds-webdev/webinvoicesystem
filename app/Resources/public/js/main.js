// resizeContentApp();
//
// $(window).resize(function () {
//     resizeContentApp();
// });
// $(document).resize(function () {
//     resizeContentApp();
// });
//
// function resizeContentApp() {
//     widthContentApp = $(window).width() - $('#application-nav').width();
//     $('.content-app').css('width', widthContentApp);
// }

var li = $(".form-group ul li");
li.css("list-style", "none").addClass("alert alert-danger");

$(document).ready(function() {

    $("#hamburger").on("click", function (e) {
        e.preventDefault();

        $("#menu-mobile").slideToggle();

    });

    $('.tr-row').on('contextmenu', function () {
        return false;
    });

    $(document).click(function (e) {
        if (e.button == 0){
            $('#contextmenu').fadeOut(100);
        }
    });

    $("a.sortable").removeAttr("title");
    $("a.asc").removeAttr("title");
    $("a.desc").removeAttr("title");

});