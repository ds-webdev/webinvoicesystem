$("#searchProducts").val("");
$("#searchProducts").on('keyup', function() {

    var input = $(this).val();
    if ( input.length >= 3 ) {
        $('#match').text('Wyszukiwanie. Proszę czekać...');
        var data = {input: input};
        $.ajax({
            type: "POST",
            url: Routing.generate('products_ajax_search'),
            data: data,
            dataType: 'json',
            timeout: 12000,
            success: function(response){

                var outPut = '';
                $('#matchList li').remove();
                if (response.status){
                    var data = $.parseJSON(response.data);
                    $.each(data, function(key,value) {
                        outPut += "<li class='search-item alert-success'>"+ value.name +"<a href="+ Routing.generate('product_update', {id: value.id}) +" class='btn-xs btn btn-warning pull-right margin-left-5'>Edycja</a>" +
                            "<a href="+ Routing.generate('product_delete', {id: value.id}) +" onclick=\"return confirm('Czy chcesz usunąć "+ value.name +" ze swojej listy produktów i usług?');\" class=\"btn btn-danger btn-xs pull-right\">Usuń</a></li>";
                    });
                    $("#matchList").append(outPut);
                    $('#match').text('');
                } else {
                    $('#matchList li').remove();
                    $('#match').text("Brak wyników dla: " + input);
                }

            },
            error: function(response) {
                console.log(response);
            }
        });
    } else {
        $('#match').text('Musisz podać minimum 3 znaki');
    }

}).blur(function () {
    $("body").on("click", function () {
        $("#matchList li").remove();
        $("#match").text("");
    });
});