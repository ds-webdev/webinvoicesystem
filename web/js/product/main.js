$('.tr-row').on('click', function (e) {
    e.preventDefault();
    window.location = Routing.generate('product_show', {id: $(this).data('id')});
});