var id = 0;
var name;

$('.tr-row').mousedown(function (e) {
    if (e.button == 2){

        $('#contextmenu').css('left', e.pageX+5);
        $('#contextmenu').css('top', e.pageY+5);
        $('#contextmenu').fadeIn(200);

        id = $(this).data('id');
        name = $(this).data('name');
    }
});

$('.tr-row').on('click', function (e) {
    e.preventDefault();
    window.location = Routing.generate('contractor_show', {id: $(this).data('id')});
});