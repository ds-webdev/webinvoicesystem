/**
 * SEARCH CONTRACTOR
 */
$("#searchContractor").val("");
$("#searchContractor").on('keyup', function() {
    var input = $(this).val();
    if ( input.length >= 3) {
        $('#match').text('Wyszukiwanie. Proszę czekać...');
        var data = {input: input};
        $.ajax({
            type: "POST",
            url: Routing.generate('contractor_ajax_search'),
            data: data,
            dataType: 'json',
            timeout: 12000,
            success: function(response){

                var outPut = '';
                $('#matchList li').remove();
                if (response.status){
                    var data = $.parseJSON(response.data);
                    $.each(data, function(key,value) {
                        outPut += "<li class='search-item alert-success'>"+ value.name +"<a href="+ Routing.generate('contractor_update', {id: value.id}) +" class='btn-xs btn btn-warning pull-right margin-left-5'>Edycja</a>" +
                            "<a href="+ Routing.generate('contractor_delete', {id: value.id}) +" onclick=\"return confirm('Czy chcesz usunąć "+ value.name +" ze swojej listy kontrahentów?');\" class=\"btn btn-danger btn-xs pull-right\">Usuń</a></li>";
                    });
                    $("#matchList").append(outPut);
                    $('#match').text('');
                } else {
                    $('#matchList li').remove();
                    $('#match').text("Brak wyników dla: " + input);
                }

            },
            error: function(response) {
                console.log("ERROR:");
                console.log(response);
            }
        });
    } else {
        $('#match').text('Musisz podać minimum 3 znaki');
    }

}).blur(function () {
    $("body").on("click", function () {
        $("#matchList li").remove();
        $("#match").text("");
    });
});
/**
 * END OF SEARCH CONTRACTOR
 */

/**
 * CONTEXTMENU AJAX
 */
$(document).click(function (e) {

    if (e.target.id && (e.target.id == "delete" || e.target.id == "edit" || e.target.id == "invoice_create")){

        var result = true;
        if (e.target.id == "delete"){
            result = confirm("Czy chcesz usunąć " + name + " ze swojej listy kontrahentów?");
        }
        if (result){
            var data = {id: id, target: e.target.id};
            $.ajax({
                type: "POST",
                url: Routing.generate('contractor_contextmenu'),
                data: data,
                dataType: 'json',
                timeout: 12000,
                success: function(response){

                    switch(response.target) {
                        case 1:
                            window.location = Routing.generate('contractor_update', {id: response.data});
                            break;
                        case 2:
                            window.location = Routing.generate('contractor_delete', {id: response.data});
                            break;
                        case 3:
                            window.location = Routing.generate('invoice_create', {id: response.data});
                            break;
                        default:
                            break;
                    }
                },
                error: function(response) {
                    console.log("ERROR");
                    console.log(response);
                }
            });
        }
    }

});
/**
 * END OF CONTEXTMENU AJAX
 */