var id = 0;
var name;
var paid;
var invoiceNumber;

$('.tr-row').mousedown(function (e) {
    if (e.button == 2) {

        id = $(this).data('id');
        name = $(this).data('name');
        paid = $(this).data('paid');
        invoiceNumber = $(this).data('number');

        var a = "<li class=\"menu-item\" id=\"set-paid\"><i class=\"glyphicon glyphicon-pencil\"></i>Oznacz jako zapłacone</li>\n" +
            "<li class=\"menu-item\" id=\"generate-pdf\"><i class=\"glyphicon glyphicon-trash\"></i>Genereuj PDF</li>\n" +
            "<li class=\"menu-item\" id=\"send-email\" data-toggle=\"modal\" data-target=\"#exampleModal\"><i class=\"glyphicon glyphicon-trash\"></i>Wyślij fakture na e-mail</li>\n" +
            "<li class=\"menu-item\" id=\"delete\"><i class=\"glyphicon glyphicon-edit\"></i>Usuń</li>" +
            "<li class=\"menu-item\" id=\"generate-again\"><i class=\"glyphicon glyphicon-edit\"></i>Wystaw ponownie</li>";

        var b = "<li class=\"menu-item\" id=\"generate-pdf\"><i class=\"glyphicon glyphicon-trash\"></i>Genereuj PDF</li>\n" +
            "<li class=\"menu-item\" id=\"send-email\" data-toggle=\"modal\" data-target=\"#exampleModal\"><i class=\"glyphicon glyphicon-trash\"></i>Wyślij fakture na e-mail</li>\n" +
            "<li class=\"menu-item\" id=\"delete\"><i class=\"glyphicon glyphicon-edit\"></i>Usuń</li>"+
            "<li class=\"menu-item\" id=\"generate-again\"><i class=\"glyphicon glyphicon-edit\"></i>Wystaw ponownie</li>";

        if (paid == 1)
            $("#contextmenu").html(b);
        else
            $("#contextmenu").html(a);

        $('#contextmenu').css('left', e.pageX+5);
        $('#contextmenu').css('top', e.pageY+5);
        $('#contextmenu').fadeIn(200);

        $("#invoice_id").val(id);

    }
});

$('.tr-row').on('click', function (e) {
    e.preventDefault();
    window.location = Routing.generate('invoice_show', {id: $(this).data('id')});
});