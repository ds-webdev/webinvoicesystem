/**
 * SEARCH CONTRACTOR ACTION
 */
$("#appbundle_invoice_customer").on('keyup', function() {

    var input = $(this).val();
    if ( input.length >= 3 ) {
        $('#match').text('Wyszukiwanie. Proszę czekać...');
        var data = {input: input};
        $.ajax({
            type: "POST",
            url: Routing.generate('contractors_for_invoice_search'),
            data: data,
            dataType: 'json',
            timeout: 12000,
            success: function(response){

                var outPut = '';
                $('#matchList li').remove();
                if (response.status){
                    var data = $.parseJSON(response.data);
                    $.each(data, function(key,value) {
                        outPut += "<li class='search-item alert-success' data-nip='"+ value.nip +"' data-id="+value.id+">" +
                            "<p>"+ value.name +", "+ value.city +", "+ value.nip+"</p>" +
                            "<span class='hidden'>"+ value.name +"</span></li>";
                    });
                    $("#matchList").append(outPut);
                    $('#match').text('');
                } else {
                    $('#matchList li').remove();
                    $('#match').text("Brak wyników dla: " + input);
                }

            },
            error: function(response) {
                console.log(response);
            }
        });
    } else {
        $('#match').text('Musisz podać minimum 3 znaki');
    }

}).blur(function () {

    $("#matchList li").on("click", function (e) {
        var contractorId = $(this).data('id');
        var contractorName = $(this).find('span').text();
        var contractorNip = $(this).data('nip');

        $("#appbundle_invoice_contractor_id").val(contractorId);
        $("#appbundle_invoice_customer").val("");
        $("#appbundle_invoice_customer").val(contractorName);
        $("#appbundle_invoice_nip").val(contractorNip);

    });

    $("body").on("click", function () {
        $("#matchList li").remove();
        $("#match").text("");
    });
});
/**
 * END OF SEARCH ACTION
 */

/**
 * SEARCH PRODUCTS ACTION
 */


$(document).on("click", ".item-search", function (e) {

    var id = e.target.id;

    $("#" + id).on('keyup', function () {

        var input = $(this).val();
        if ( input.length >= 3 ) {
            $('#matchForList').text('Wyszukiwanie. Proszę czekać...');
            var data = {input: input};
            $.ajax({
                type: "POST",
                url: Routing.generate('products_for_invoice_search'),
                data: data,
                dataType: 'json',
                timeout: 12000,
                success: function(response){

                    var outPut = '';
                    $('#matchItems li').remove();
                    if (response.status){

                        if (response.data != undefined){
                            var data = $.parseJSON(response.data);
                            $.each(data, function(key,value) {
                                outPut += "<li class='search-item alert-success' data-input='"+ id +"' data-price='"+ value.price +"' data-tax='"+ value.tax +"' data-id="+value.id+">" +
                                    "<p>"+ value.name +"</p></li>";
                            });

                            $('#matchForList').text('');
                            $("#"+id).after('<ul id="matchItems" class="search-ajax-list">'+outPut+'</ul>');
                        } else {
                            $('#matchForList').text('Brak wyników dla ' + input);
                        }


                    } else {
                        $('#matchItems li').remove();
                        $('#matchForList').text("Brak wyników dla: " + input);
                    }

                },
                error: function(response) {
                    console.log(response);
                }
            });

        } else {
            $('#matchForList').text('Musisz podać minimum 3 znaki');
        }


    }).blur(function () {
        $(".search-item").on("click" ,function () {

            var str = id.replace("appbundle_invoice_items_", "");
            var count = str.replace("_name", "");

            var productId = $(this).data('id');
            var productName = $(this).text();
            var productNetPrice = $(this).data('price');
            var productTax = $(this).data('tax');

            var gorssPrice = productNetPrice;

            if (productTax > 0)
                gorssPrice = (productNetPrice * productTax) + productNetPrice;

            var inputNameId = "#appbundle_invoice_items_" + count + "_name";
            var inputNetPriceId = "#appbundle_invoice_items_" + count + "_netPrice";
            var inputGrossPriceId = "#appbundle_invoice_items_" + count + "_grossPrice";
            var inputTaxId = "#appbundle_invoice_items_" + count + "_tax";

            if (productTax == 0.05)
                productTax = "5%";
            else if (productTax == 0.08)
                productTax = "8%";
            else if (productTax == 0.23)
                productTax = "23%";

            $(inputNameId).val(productName);
            $(inputNetPriceId).val(productNetPrice);
            $(inputGrossPriceId).val(gorssPrice);
            $(inputTaxId).val(productTax);

        });

        $("body").on("click", function () {
            $("#matchItems").remove();
            $("#matchForList").text("");
        });
    });

});
/**
 * END OF SEARCH ACTION
 */

/**
 * CONTEXMENU ACTIONS
 **/

$(document).on('click', function (e) {

    if (e.target.id && (e.target.id == "set-paid" || e.target.id == "generate-pdf" ||
            e.target.id == "send-email" || e.target.id == "delete" || e.target.id == "generate-again")){

        var result = true;

        if(e.target.id == "delete")
            result = confirm("Czy chcesz usunąć fakture o numerze: " + invoiceNumber + " ?");

        if (result){

            var data = {id: id, target: e.target.id};

            $.ajax({
               type: "POST",
                url: Routing.generate('invoice_contextmenu'),
                data: data,
                dataType: 'json',
                timeout: 12000,
                success: function (response) {

                   var invoiceId = response.data;

                   switch (response.target){
                       case 0: //pdf
                           window.location = Routing.generate('invoice_pdf', {id: invoiceId})
                           break;
                       case 1: //delete
                           window.location = Routing.generate('invoice_delete', {id: invoiceId});
                           break;
                       case 2: //again
                           window.location = Routing.generate('invoice_again', {id: invoiceId});
                           break;
                       case 3: //paid
                           window.location = Routing.generate('invoice_paid', {id: invoiceId});
                           break;
                       case 4: //mail
                           break;
                       default:
                           break;
                   }

                },
                error: function (response) {
                    console.log("ERROR");
                    console.log(response);
                }
            });


        }
    }

});

/**
 * END OF CONTEXMENU ACTIONS
 **/

/**
 * SAVE CUSTOMER AJAX
 */

$('#save-customer').on('click', function (e) {

    var data = {
        name: $('#customer-name').val(),
        nip: $('#customer-nip').val(),
        zipCode: $('#customer-zip-code').val(),
        city: $('#customer-city').val(),
        street: $('#customer-street').val()
    };

    $.ajax({
        type: "POST",
        url: Routing.generate('create_contractor_from_invoice'),
        data: data,
        dataType: 'json',
        timeout: 12000,
        success: function (response) {
            $("#appbundle_invoice_nip").val(response.nip);
            $("#appbundle_invoice_customer").val(response.name);
            $("#appbundle_invoice_contractor_id").val(response.id);
            $('#exampleModal').modal('hide');
        },
        error: function (response) {
            console.log('ERROR');
            console.log(response);
            $('#exampleModal').modal('hide');
        }
    });

    $('#exampleModal').modal('hide');
});

/**
 * END OF SAVE CUSTOMER AJAX
 */
/**
 * SEARCH INVOICE
 */

$("#searchInvoice").on("keyup", function () {

    var input = $(this).val();

    if (input.length >= 3){
        $("#match").text("");

        $.ajax({
            type: "POST",
            url: Routing.generate('invoice_search'),
            data: {input: input},
            dataType: 'json',
            timeout: 12000,
            success: function (response) {
                var outPut = '';
                for(var i = 0; i < response.length; i++){
                    outPut += "<li class='search-item alert-success'>"
                        + response[i]['customer'] + ", " + response[i]['invoiceNumber'] +
                        "<a class='btn btn-success btn-xs margin-left-5 pull-right' href='do-dodania'>PDF</a>" +
                        "<a class='btn btn-danger btn-xs pull-right' href='"+ Routing.generate('invoice_delete', {id: response[i]['id']}) +"'>Usuń</a></li>"
                }
                $("#matchList li").remove();
                $("#matchList").append(outPut);
            },
            error: function (response) {
                console.log("ERROR");
                console.log(response);
                $("#match").text("Brak wyników");
            }
        });

    } else {
        $("#match").text("Musisz podać minimum 3 znaki");
    }

}).blur(function () {

    $("body").on("click", function () {
        $("#match").text("");
        $("#matchList li").remove();
    });

});

/**
 * END OF SEARCH INVOICE
 */