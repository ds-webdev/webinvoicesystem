<?php

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class ContainsNip extends Constraint {

    public $message = "Numer NIP jest nie prawidłowy";

    public function validatedBy() {
        return get_class($this).'Validator';
    }

}