<?php

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsNipValidator extends ConstraintValidator {

    public function validate($value, Constraint $constraint) {

        $value = preg_replace('/[^0-9]+/', '', $value);

        if (strlen($value) !== 10){
            return $this->context->buildViolation('Numer nip musi składać się z 10 cyfr')
                ->addViolation();
        }

        $steps = [6, 5, 7, 2, 3, 4, 5, 6, 7];
        $sum = 0;

        for($i = 0; $i < 9; $i++)
            $sum += $steps[$i] * $value[$i];

        $sum = $sum % 11;
        $controlNumber = $sum === 10 ? 0 : $sum;

        if ($controlNumber != $value[9]){
            return $this->context->buildViolation('Błędny numer nip')
                ->addViolation();
        }

        return true;
    }

}