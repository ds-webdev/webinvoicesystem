<?php

namespace AppBundle\Form;

use AppBundle\Validator\Constraints\ContainsNip;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContractorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nip', NumberType::class, array(
                'label' => 'NIP',
                'required' => FALSE,
                'attr' => array(
                    'class' => 'form-control validate[required, custom[nip]]',
                    'placeholder' => '0009998877',
                ),
                'constraints' => array(
                    new ContainsNip()
                )
            ))
            ->add('name', TextType::class, array(
                'label' => 'Nazwa firmy/imię i nazwisko',
                'required' => FALSE,
                'attr' => array(
                    'placeholder' => 'Podaj nazwę...',
                    'class' => 'form-control validate[required]',
                ),
            ))
            ->add('zipCode', TextType::class, array(
                'label' => 'Kod pocztowy',
                'required' => FALSE,
                'attr' => array(
                    'placeholder' => '00-000',
                    'class' => 'validate[required, custom[zip]] form-control',
                ),
            ))
            ->add('city', TextType::class, array(
                'label' => 'Miejscowość',
                'required' => FALSE,
                'attr' => array(
                    'class' => 'validate[required] form-control',
                    'placeholder' => 'Podaj miejscowosc...',
                ),
            ))
            ->add('street', TextType::class, array(
                'label' => 'Ulica nr domu / nr lokalu',
                'required' => FALSE,
                'attr' => array(
                    'class' => 'form-control validate[required]',
                    'placeholder' => 'ulica nr domu / nr lokalu',
                )
            ))
            ->add('phone', NumberType::class, array(
                'label' => 'Telefon',
                'required' => FALSE,
                'attr' => array(
                    'class' => 'validate[custom[phone]] form-control',
                    'placeholder' => '782539080',
                ),
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Contractor'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_contractor';
    }


}
