<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class ItemType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required' => FALSE,
                'label' => 'Nazwa produktu lub usługi',
                'attr' => array(
                    'class' => 'form-search item-search',
                    'autocomplete' => 'off',
                ),
                'constraints' => array(
                    new NotBlank(['message' => 'To pole jest wymagane, musisz podać nazwę produktu/usługi'])
                ),
            ))
            ->add('netPrice', MoneyType::class, array(
                'label' => 'Cena netto',
                'required' => TRUE,
                'currency' => '',
                'attr' => array(
                    'class' => 'invoice-item netPrice'
                ),
                'constraints' => array(
                    new NotBlank(['message' => 'To pole jest wymagane, musisz podać cene netto']),
                    new Range(array(
                        'min' => 1,
                        'minMessage' => 'Minimalna cena to 1zł',
                    ))
                ),
            ))
            ->add('tax', ChoiceType::class, array(
                'label' => 'Podatek',
                'required' => TRUE,
                'attr' => array(
                    'class' => 'invoice-item tax'
                ),
                'constraints' => array(
                    new NotBlank(['message' => 'To pole jest wymagane, musisz podać podatek'])
                ),
                'choices' => array(
                    '0%' => '0',
                    '5%' => '5%',
                    '8%' => '8%',
                    '23%' => '23%',
                ),
            ))
            ->add('amount', IntegerType::class, array(
                'label' => 'Ilość',
                'required' => TRUE,
                'attr' => array(
                    'class' => 'invoice-item amount'
                ),
                'constraints' => array(
                    new NotBlank(['message' => 'To pole jest wymagane, musisz podać ilość']),
                    new Range(array(
                        'min' => 1,
                        'minMessage' => 'Minimalna ilość to 1',
                        'invalidMessage' => 'To pole jest wymagane, musisz podać ilość'
                    )),
                ),
            ))
            //            ->add('grossPrice', MoneyType::class, array(
            //                'label' => 'Cena brutto',
            //                'currency' => '',
            //                'required' => TRUE,
            //                'attr' => array(
            //                    'class' => 'invoice-item grossPrice'
            //                ),
            //                'constraints' => array(
            //                    new NotBlank(['message' => 'To pole jest wymagane, musisz podać cene brutto'])
            //                ),
            //
            //            ))
            ->add('unit', TextType::class, array(
                'label' => 'Jednostka',
                'required' => TRUE,
                'attr' => array(
                    'class' => 'invoice-item amount'
                ),
                'constraints' => array(
                    new NotBlank(['message' => 'To pole jest wymagane, musisz podać jednostkę'])
                ),
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Item'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_item';
    }


}
