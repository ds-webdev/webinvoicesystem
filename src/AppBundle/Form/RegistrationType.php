<?php
/**
 * Created by PhpStorm.
 * User: dsiko
 * Date: 27.09.2017
 * Time: 13:02
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('username', TextType::class, array(
                'label' => 'form.username',
                'translation_domain' => 'FOSUserBundle',
                'required' => FALSE,
                'attr' => array(
                    'placeholder' => 'Podaj nazwe użytkownika...',
                    'class' => 'form-control validate[required]',
                ),
            ))
            ->add('email', TextType::class, array(
                'label' => 'form.email',
                'translation_domain' => 'FOSUserBundle',
                'required' => FALSE,
                'attr' => array(
                    'placeholder' => 'Podaj e-mail...',
                    'class' => 'form-control validate[required, custom[email]]',
                )
            ))
            ->add('name', TextType::class, array(
                'label' => 'Nazwa firmy/imię i nazwisko',
                'required' => FALSE,
                'attr' => array(
                    'placeholder' => 'Podaj nazwę...',
                    'class' => 'form-control validate[required]',
                ),
            ))
            ->add('nip', NumberType::class, array(
                'label' => 'NIP',
                'required' => FALSE,
                'attr' => array(
                    'class' => 'form-control validate[required, custom[nip]]',
                    'placeholder' => '0009998877',
                ),
            ))
            ->add('zipCode', TextType::class, array(
                'label' => 'Kod pocztowy',
                'required' => FALSE,
                'attr' => array(
                    'placeholder' => '00-000',
                    'class' => 'validate[required, custom[zip]] form-control',
                ),
            ))
            ->add('city', TextType::class, array(
                'label' => 'Miejscowość',
                'required' => FALSE,
                'attr' => array(
                    'class' => 'validate[required] form-control',
                    'placeholder' => 'Podaj miejscowosc...',
                ),
            ))
            ->add('street', TextType::class, array(
                'label' => 'Ulica nr domu / nr lokalu',
                'required' => FALSE,
                'attr' => array(
                    'class' => 'form-control validate[required]',
                    'placeholder' => 'ulica nr domu / nr lokalu',
                )
            ))
            ->add('phone', NumberType::class, array(
                'label' => 'Telefon',
                'required' => FALSE,
                'attr' => array(
                    'class' => 'validate[custom[phone]] form-control',
                    'placeholder' => '782539080',
                ),
            ))
            ->add('bank', TextType::class, array(
                'label' => 'Nazwa banku',
                'required' => FALSE,
                'attr' => array(
                    'class' => 'form-control ',
                    'placeholder' => 'Podaj nazwe banku...'
                )
            ))
            ->add('accountNumber', NumberType::class, array(
                'label' => 'Numer konta bankowego',
                'required' => FALSE,
                'attr' => array(
                    'placeholder' => 'Podaj numer konta bankowego...',
                    'class' => 'form-control validate[custom[account]]'
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class' => 'btn btn-success',
                ),
            ))
            ;

    }

    public function getParent() {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix() {
        return 'app_user_registration';
    }

}