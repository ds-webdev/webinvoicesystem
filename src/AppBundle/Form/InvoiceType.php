<?php

namespace AppBundle\Form;

use AppBundle\Validator\Constraints\ContainsNip;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class InvoiceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('invoiceNumber', TextType::class, array(
                'label' => 'Numer faktury: ',
                'attr' => array(
                    'class' => 'only-read-form'
                )
            ))
            ->add('customer', TextType::class, array(
                'mapped' => FALSE,
                'required' => TRUE,
                'label' => 'Odbiorca',
                'attr' => array(
                    'autocomplete' => 'off',
                    'class' => 'form-search validate[required]',
                    'placeholder' => 'Podaj nazwe, nip lub miejscowość',
                )
            ))
            ->add('contractor_id', IntegerType::class, array(
                'mapped' => FALSE,
                'required' => FALSE,
                'label_attr' => array(
                    'class' => 'hidden',
                ),
                'attr' => array(
                    'class' => 'hidden',
                ),
            ))
            ->add('nip', TextType::class, array(
                'label' => 'NIP: ',
                'mapped' => FALSE,
                'attr' => array(
                    'class' => 'only-read-form'
                )
            ))
            ->add('items', CollectionType::class, array(
                'entry_type' => ItemType::class,
                'allow_add' => TRUE,
                'allow_delete' => TRUE,
                'prototype' => TRUE,
                'label' => FALSE,
                'required' => TRUE,
                'attr' => array(
                    'multiple' => TRUE,
                ),
            ))
            ->add('paymentMethod', ChoiceType::class, array(
                'choices' => array(
                    'Wybierz opcje' => '',
                    'Gotówka' => 1,
                    'Przelew' => 2,
                ),
                'label' => 'Metoda płatności',
                'required' => TRUE,
                'attr' => array(
                    'class' => 'form-select validate[required]'
                ),
            ))
            ->add('paid', ChoiceType::class, array(
                'required' => TRUE,
                'label' => 'Czy zapłacono?',
                'choices' => array(
                    'Wybierz opcje' => '',
                    'Zapłacono' => 1,
                    'Oczekuje na zapłate' => 0
                ),
                'attr' => array(
                    'class' => 'form-select validate[required]'
                ),
            ))
            ->add('dateOfIssue', TextType::class, array(
                'label' => 'Data wystawienia faktury',
                'required' => TRUE,
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('saleDate', TextType::class, array(
                'label' => 'Data sprzedaży',
                'required' => TRUE,
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('dateOfPayment', TextType::class, array(
                'label' => 'Data płatności',
                'required' => TRUE,
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Wystaw fakture',
                'attr' => array(
                    'class' => 'btn btn-success'
                ),
            ))
        ;

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Invoice'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_invoice';
    }

}
