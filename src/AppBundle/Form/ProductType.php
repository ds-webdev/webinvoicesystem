<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa',
                'required' => TRUE,
                'attr' => array(
                  'placeholder' => 'Podaj nazwę produktu\usługi...',
                  'class' => 'form-control validate[required]',
                ),
            ))
            ->add('price', NumberType::class, array(
                'label' => 'Cena netto (należy podać samą liczbę bez waluty. Domyślna waluta to PLN)',
                'required' => FALSE,
                'attr' => array(
                    'placeholder' => 'Podaj cenę...',
                    'class' => 'form-control'
                )
            ))
            ->add('tax', ChoiceType::class, array(
                'label' => 'VAT',
                'required' => FALSE,
                'placeholder' => 'Wybierz podatek VAT',
                'choices' => array(
                    '0%' => 0,
                    '5%' => 0.05,
                    '8%' => 0.08,
                    '23%' => 0.23,
                ),
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_product';
    }


}
