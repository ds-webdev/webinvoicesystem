<?php

namespace AppBundle\Repository;
use AppBundle\Entity\User;


/**
 * ContractorRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ContractorRepository extends \Doctrine\ORM\EntityRepository {

    public function ajaxSearch(User $user, $colName, $expression){

        return $this->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->where('c.user = :user')
            ->andWhere('c.' . $colName . ' LIKE :expression')
            ->setParameter('user', $user)
            ->setParameter('expression', '%'. $expression . '%')
            ->getQuery()
            ->getResult();
    }

    public function searchContractors(User $user, $expression){

        return $this->createQueryBuilder('c')
            ->select('c.id, c.name, c.nip, c.city')
            ->where('c.user = :user')
            ->andWhere('(c.name LIKE :expression OR c.nip LIKE :expression OR c.city LIKE :expression)')
            ->setMaxResults(50)
            ->setParameter('user', $user)
            ->setParameter('expression', '%'. $expression . '%')
            ->getQuery()
            ->getResult();
    }

}
