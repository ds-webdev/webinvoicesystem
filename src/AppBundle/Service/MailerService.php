<?php
/**
 * Created by PhpStorm.
 * User: dsiko
 * Date: 15.02.2018
 * Time: 12:11
 */

namespace AppBundle\Service;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class MailerService{

    private $mailer;

    public function __construct(PHPMailer $mailer, $password) {
        $this->mailer = $mailer;
        $this->init($password);
    }

    private function init($password){
        $this->mailer->SMTPDebug = 2;
//        $this->mailer->isSMTP();
        $this->mailer->Host = "smtp.gmail.com";
        $this->mailer->SMTPAuth = true;
        $this->mailer->Username = 'dsikorski.praca@gmail.com';
        $this->mailer->Password = $password;
        $this->mailer->SMTPSecure = 'tls';
        $this->mailer->Port = 587;
        $this->mailer->isHTML(FALSE);
        $this->mailer->setLanguage('pl');
        $this->mailer->CharSet = "UTF-8";
    }

    public function setFrom($mail, $name, $desc){
        try{
            $this->mailer->setFrom($mail, $desc);
            $this->mailer->addReplyTo($mail, $name);
        } catch (Exception $e){
            echo 'SetFrom: ' . $e->getMessage();
        }
    }

    public function setAddress($address, $name = ''){
        $this->mailer->addAddress($address, $name);
    }

    public function setContent($subject, $body){
        $this->mailer->Subject = $subject;
        $this->mailer->Body = $body;
    }

public function addAttachment($path, $name){
    try{
        return $this->mailer->addAttachment($path, $name);
    } catch (Exception $e){
        $message = 'Message '.$e->getMessage().
            ' in file '.$e->getFile().
            ' on line '.$e->getLine();
        error_log($message);
    }

    return false;
}

public function send(){
    try{
         return $this->mailer->send();
    } catch (Exception $e){
        $message = 'Message '.$e->getMessage().
            ' in file '.$e->getFile().
            ' on line '.$e->getLine();
        error_log($message);
    }

    return false;
}
}