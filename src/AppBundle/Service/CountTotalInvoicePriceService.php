<?php

namespace AppBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;

class CountTotalInvoicePriceService {

    private $totalNetPrice;
    private $totalGrossPrice;

    public function __construct(){
        $this->totalNetPrice = 0;
        $this->totalGrossPrice = 0;
    }

    /**
     * Return total net price
     *
     * @param ArrayCollection $items
     *
     * @return float
     */
    public function getTotalNetPrice(ArrayCollection $items) : float {

        foreach ($items as $item)
            $this->totalNetPrice += $item->getNetPrice() * $item->getAmount();

        return $this->totalNetPrice;
    }

    /**
     * Return total gross price
     *
     * @param ArrayCollection $items
     *
     * @return float
     */
    public function getTotalGrossPrice(ArrayCollection $items) : float {

        foreach ($items as $item){
            $tax = $item->getTax();

            if ($tax == "23%")
                $tax = 0.23;
            elseif ($tax == "8%")
                $tax = 0.08;
            elseif ($tax == "5%")
                $tax = 0.05;
            else
                $tax = 0;

            if ($tax == 0)
                $this->totalGrossPrice += $item->getNetPrice() * $item->getAmount();
            else
                $this->totalGrossPrice += ($item->getNetPrice() + ($item->getNetPrice() * $tax)) * $item->getAmount();
        }

//        var_dump($this->totalGrossPrice);die;
        return $this->totalGrossPrice;
    }

}