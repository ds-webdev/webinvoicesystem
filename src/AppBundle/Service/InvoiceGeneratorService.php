<?php
/**
 * Created by PhpStorm.
 * User: dsiko
 * Date: 21.01.2018
 * Time: 13:30
 */

namespace AppBundle\Service;


use AppBundle\Entity\Contractor;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Invoice;
use AppBundle\Entity\Item;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

class InvoiceGeneratorService {

    private $em;

    private $invoice;

    private $items;

    private $numberGenerator;

    private $countPrice;

    public function __construct(EntityManager $entityManager, InvoiceNumberGeneratorService $invoiceNumberGeneratorService, CountTotalInvoicePriceService $invoicePriceService) {
        $this->em = $entityManager;
        $this->numberGenerator = $invoiceNumberGeneratorService;
        $this->countPrice = $invoicePriceService;
    }

    public function setInvoice(Invoice &$invoice){
        $this->invoice = $invoice;
    }

    public function setItems(ArrayCollection $items){
        $this->items = $items;
    }

    public function addItems($items){
        $this->items = $items;
        foreach ($items as $item)
            $this->invoice->addItem($item);
    }

    public function generateNumber(){

        $invoice = $this->em->getRepository(Invoice::class)
            ->findOneBy(['user'=> $this->invoice->getUser()], ['id' => 'DESC']);

        $this->numberGenerator->setOldNumber($invoice);
        $this->invoice->setInvoiceNumber($this->numberGenerator->generateNewNumber());
    }

    public function makeCustomer(Contractor $contractor = NULL){

        $customer = new Customer();

        if ($contractor !== NULL){
            $customer->setName($contractor->getName());
            $customer->setNip($contractor->getNip());
            $customer->setZpiCode($contractor->getZipCode());
            $customer->setCity($contractor->getCity());
            $customer->setStreet($contractor->getStreet());
        }

        $customer->setInvoice($this->invoice);
        $this->invoice->setCustomer($customer);
    }

    public function makeCustomerFromExistInvoice(Customer $oldCustomer = NULL){

        $customer = new Customer();

        if ($oldCustomer !== NULL){
            $customer->setName($oldCustomer->getName());
            $customer->setNip($oldCustomer->getNip());
            $customer->setZpiCode($oldCustomer->getZpiCode());
            $customer->setCity($oldCustomer->getCity());
            $customer->setStreet($oldCustomer->getStreet());
        }

        $customer->setInvoice($this->invoice);
        $this->invoice->setCustomer($customer);
    }

    public function makeDateTimeFromString(string $date) : \DateTime{
        return new \DateTime(
            \DateTime::createFromFormat("d/m/Y", $date)
            ->format("d-m-Y H:i:s")
        );
    }

    public function makeTotalNetPrice(){
        $totalNetPrice = $this->countPrice->getTotalNetPrice($this->items);
        $this->invoice->setTotalNetPrice($totalNetPrice);
    }

    public function makeTotalGrossPrice(){
        $totalGrossPrice = $this->countPrice->getTotalGrossPrice($this->items);
        $this->invoice->setTotalGrossPrice($totalGrossPrice);
    }

    public function generateItems(){

        foreach ($this->items as $item){

            $item->setInvoice($this->invoice);
            $tax = $item->getTax();

            if ($tax == "23%")
                $tax = 0.23;
            elseif ($tax == "8%")
                $tax = 0.08;
            elseif ($tax == "5%")
                $tax = 0.05;
            else
                $tax = 0;

            if ($tax == 0)
                $grossPrice = $item->getNetPrice() * $item->getAmount();
            else
                $grossPrice = ($item->getNetPrice() + ($item->getNetPrice() * $tax)) * $item->getAmount();


            $item->setGrossPrice($grossPrice);
//            $this->invoice->addItem($item);
            $this->em->persist($item);
        }
    }

}