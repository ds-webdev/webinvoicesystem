<?php
/**
 * Created by PhpStorm.
 * User: dsiko
 * Date: 22.11.2017
 * Time: 18:36
 */

namespace AppBundle\Service;


class InvoiceNumberGeneratorService {

    private $oldNumber;

    private $oldCount;

    private $oldMonth;

    private $oldYear;

    private $actualCount;

    private $actualMonth;

    private $actualYear;



    public function __construct() {
        $this->generateActualParam();
    }

    public function setOldNumber($oldNumber = NULL){
        if ($oldNumber === NULL)
            $this->oldNumber = $this->generateOldParam(NULL);
        else
            $this->oldNumber = $this->generateOldParam($oldNumber->getInvoiceNumber());
    }

    private function generateActualParam(){
        $dateArray = getdate();
        $this->actualCount = 0;
        $this->actualMonth = $dateArray['mon'];
        $this->actualYear = $dateArray['year'];
    }

    private function generateOldParam($oldNumber){
        if (NULL !== $oldNumber){
            $oldNumber = explode('/', $oldNumber);

            $this->oldCount = $oldNumber[0];
            $this->oldMonth = $oldNumber[1];
            $this->oldYear = $oldNumber[2];

            return TRUE;
        }

        $this->oldCount = 0;
        $this->oldMonth = 0;
        $this->oldYear = 0;

        return FALSE;
    }

    private function checkMonth(){
        return ($this->actualMonth == $this->oldMonth) ? TRUE : FALSE;
    }

    private function checkYear(){
        return ($this->actualYear == $this->oldYear) ? TRUE : FALSE;
    }

    public function generateNewNumber(){

        if ($this->oldNumber && $this->checkYear() && $this->checkMonth()){
            $this->actualCount = $this->oldCount + 1;

            return $this->actualCount . '/' . $this->actualMonth . '/' . $this->actualYear;
        } else {
            $this->actualCount = 1;

            return $this->actualCount . '/' . $this->actualMonth . '/' . $this->actualYear;
        }

    }

}