<?php
/**
 * Created by PhpStorm.
 * User: dsiko
 * Date: 08.11.2017
 * Time: 22:52
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Contractor;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadUserData extends Fixture{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager) {

        $faker = Factory::create();

//        $userManager = $this->container->get('fos_user.user_manager');


        $user = new User();
        $user->setEnabled(TRUE);
        $user->setUsername('dawid');
        $user->setName('DS-WebDev Usługi programistyczne');
        $user->setPlainPassword('test');
        $user->setEmail('dawid@ds-webdev.pl');
        $user->setNip(1098765432);
        $user->setCity('Secemin');
        $user->setZipCode('29-145');
        $user->setStreet('Kościuszki 39');
        $user->setPhone(782539080);
        $user->setBank('PKO');
        $user->setAccountNumber('31 1020 2313 0000 3502 0502 7471');

        $manager->persist($user);


//        for ($i = 0; $i < 1001; $i++){
//            $product = new Product();
//            $product->setName($faker->sentence(6, TRUE));
//            $product->setUser($user);
//            $product->setTax($faker->randomElement(array(0, 5, 8, 23)));
//            $product->setPrice($faker->numberBetween(0, 99999999));
//            $manager->persist($product);
//        }
//
//        for ($i = 0; $i < 1001; $i++){
//            $contractor = new Contractor();
//            $contractor->setUser($user);
//            $name = $faker->firstName . ' ' . $faker->lastName;
//            $contractor->setName($name);
//            $contractor->setNip(1098765432);
//            $contractor->setCity($faker->city);
//            $contractor->setZipCode($faker->postcode);
//            $contractor->setStreet($faker->streetName);
//            $contractor->setPhone($faker->numberBetween(111111111, 999999999));
//            $manager->persist($contractor);
//        }

        $manager->flush();

    }
}