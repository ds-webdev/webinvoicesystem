<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Customer
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Invoice", mappedBy="customer")
     */
    private $invoice;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @var int
     * @ORM\Column(name="nip", type="integer", nullable=false)
     *
     * @Assert\NotBlank(message="To pole jest wymagane, musisz podać NIP", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=10,
     *     max=10,
     *     minMessage="Numer NIP musi zawierać 10 cyfr",
     *     maxMessage="Numer NIP musi zawierać 10 cyfr",
     *     groups={"Registration", "Profile"}
     * )
     */
    private $nip;

    /**
     * @var string
     * @ORM\Column(name="zpi_code", type="string", nullable=false)
     */
    private $zpiCode;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", nullable=false)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="street", type="string", nullable=false)
     */
    private $street;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nip
     *
     * @param integer $nip
     *
     * @return Customer
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip
     *
     * @return integer
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Set zpiCode
     *
     * @param string $zpiCode
     *
     * @return Customer
     */
    public function setZpiCode($zpiCode)
    {
        $this->zpiCode = $zpiCode;

        return $this;
    }

    /**
     * Get zpiCode
     *
     * @return string
     */
    public function getZpiCode()
    {
        return $this->zpiCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Customer
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Customer
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Add invoice
     *
     * @param \AppBundle\Entity\Invoice $invoice
     *
     * @return Customer
     */
    public function setInvoice(\AppBundle\Entity\Invoice $invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }


    public function getInvoice()
    {
        return $this->invoice;
    }

    public function __toString() {
        return $this->name;
    }
}
