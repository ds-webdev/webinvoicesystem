<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Product", mappedBy="user")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Contractor", mappedBy="user")
     */
    private $contractors;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Invoice", mappedBy="user")
     */
    private $invoices;

    /**
     * @var string
     * @ORM\Column(name="name", type="text", nullable=false)
     *
     * @Assert\NotBlank(message="To pole jest wymagane, musisz podać nazwę firmy lub imię i nazwisko", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     minMessage="Nazwa jest za krótka",
     *     groups={"Registration", "Profile"}
     * )
     */
    private $name;

    /**
     * @var int
     * @ORM\Column(name="nip", type="integer", nullable=false)
     *
     * @Assert\NotBlank(message="To pole jest wymagane, musisz podać NIP", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=10,
     *     max=10,
     *     minMessage="Numer NIP musi zawierać 10 cyfr",
     *     maxMessage="Numer NIP musi zawierać 10 cyfr",
     *     groups={"Registration", "Profile"}
     * )
     */
    private $nip;

    /**
     * @var string
     * @ORM\Column(name="zip_code", type="string", length=6, nullable=false)
     *
     * @Assert\NotBlank(message="Kod pocztowy jest wymagany")
     *
     */
    private $zipCode;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="To pole jest wymagane, musisz podać nazwę miejscowości")
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="street", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="To pole jest wymagane, musisz podać nazwę ulicy oraz numer domu / lokalu")
     */
    private $street;

    /**
     * @var int

     * @ORM\Column(name="phone", type="integer", nullable=true)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="bank", type="string", length=255, nullable=true)
     */
    private $bank;

    /**
     * @var string
     * @ORM\Column(name="account_number", type="string", nullable=true)
     */
    private $accountNumber;

    public function __construct() {
        parent::__construct();
        $this->roles = array(UserInterface::ROLE_DEFAULT);
        $this->products = new ArrayCollection();
        $this->contractors = new ArrayCollection();
        $this->invoices = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set nip
     *
     * @param int $nip
     *
     * @return User
     */
    public function setNip($nip) {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip
     *
     * @return int
     */
    public function getNip() {
        return $this->nip;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     *
     * @return User
     */
    public function setZipCode($zipCode) {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode() {
        return $this->zipCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return User
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set bank
     *
     * @param string $bank
     *
     * @return User
     */
    public function setBank($bank)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank
     *
     * @return string
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set accountNumber
     *
     * @param string $accountNumber
     *
     * @return User
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Add product
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return User
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \AppBundle\Entity\Product $product
     */
    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add contractor
     *
     * @param \AppBundle\Entity\Contractor $contractor
     *
     * @return User
     */
    public function addContractor(Contractor $contractor)
    {
        $this->contractors[] = $contractor;

        return $this;
    }

    /**
     * Remove contractor
     *
     * @param \AppBundle\Entity\Contractor $contractor
     */
    public function removeContractor(Contractor $contractor)
    {
        $this->contractors->removeElement($contractor);
    }

    /**
     * Get contractors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractors()
    {
        return $this->contractors;
    }

    /**
     * Add invoice
     *
     * @param \AppBundle\Entity\Invoice $invoice
     *
     * @return User
     */
    public function addInvoice(Invoice $invoice)
    {
        $this->invoices[] = $invoice;

        return $this;
    }

    /**
     * Remove invoice
     *
     * @param \AppBundle\Entity\Invoice $invoice
     */
    public function removeInvoice(Invoice $invoice)
    {
        $this->invoices->removeElement($invoice);
    }

    /**
     * Get invoices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvoices()
    {
        return $this->invoices;
    }
}
