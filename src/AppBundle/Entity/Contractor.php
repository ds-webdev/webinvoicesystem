<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contractor
 *
 * @ORM\Table(name="contractor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContractorRepository")
 */
class Contractor {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="contractors")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var int
     * @ORM\Column(name="nip", type="integer", nullable=false)
     *
     * @Assert\NotBlank(message="To pole jest wymagane, musisz podać NIP", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=10,
     *     max=10,
     *     minMessage="Numer NIP musi zawierać 10 cyfr",
     *     maxMessage="Numer NIP musi zawierać 10 cyfr",
     *     groups={"Registration", "Profile"}
     * )
     */
    private $nip;

    /**
     * @var string
     * @ORM\Column(name="name", type="text", nullable=false)
     *
     * @Assert\NotBlank(message="To pole jest wymagane, musisz podać nazwę firmy lub imię i nazwisko")
     * @Assert\Length(
     *     min=3,
     *     minMessage="Nazwa jest za krótka"
     * )
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="zip_code", type="string", length=6, nullable=false)
     *
     * @Assert\NotBlank(message="Kod pocztowy jest wymagany")
     *
     */
    private $zipCode;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="To pole jest wymagane, musisz podać nazwę miejscowości")
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="street", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="To pole jest wymagane, musisz podać nazwę ulicy oraz numer domu / lokalu")
     */
    private $street;

    /**
     * @var int

     * @ORM\Column(name="phone", type="integer", nullable=true)
     */
    private $phone;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nip
     *
     * @param integer $nip
     *
     * @return Contractor
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip
     *
     * @return integer
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Contractor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     *
     * @return Contractor
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Contractor
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Contractor
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set phone
     *
     * @param integer $phone
     *
     * @return Contractor
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return integer
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Contractor
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get full Contractor address
     *
     * @return string
     */
    public function getAddress(){
        return $this->getZipCode() . ' ' . $this->getCity() . ', ' . $this->getStreet();
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getName();
    }

}
