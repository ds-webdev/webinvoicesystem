<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contractor;
use AppBundle\Entity\Invoice;
use AppBundle\Entity\Product;
use AppBundle\Form\InvoiceType;
use AppBundle\Service\MailerService;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class InvoiceController extends Controller {

    /**
     * @Route("/aplikacja/faktury/{id}", name="invoice_show")
     *
     * @param Invoice|NULL $invoice
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Invoice $invoice = null, Request $request){

        if ($invoice == null || $invoice->getDelete() == 1){
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Faktura nie istnieje');

            return $this->redirectToRoute('invoice_index');
        }

        $user = $this->getUser();
        if ($user != $invoice->getUser()){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        return $this->render('application/invoice/show.html.twig', array(
            'invoice' => $invoice
        ));
    }

    /**
     * @Route("/aplikacja/faktury/nowa-faktura/{id}", defaults={"id" = null}, name="invoice_create")
     *
     * @param Contractor|null $contractor
     * @param Request $request
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Contractor $contractor = NULL, Request $request){

        $user = $this->getUser();

        if (!$user){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $em = $this->getDoctrine()->getManager();

        $requestArray = $request->request->all();
        if (isset($requestArray['appbundle_invoice']['contractor_id'])){
            $contractorId = $requestArray['appbundle_invoice']['contractor_id'];
            $contractor = $em->getRepository(Contractor::class)
                ->find(intval($contractorId));
        }

        $invoiceGenerator = $this->get('app.invoice_generator');

        $invoice = new Invoice($user);
        $invoiceGenerator->setInvoice($invoice);
        $invoiceGenerator->generateNumber();
        $invoiceGenerator->makeCustomer($contractor);

        $form = $this->createForm(InvoiceType::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $invoice->setDateOfIssue($invoiceGenerator->makeDateTimeFromString(
                $form->get('dateOfIssue')->getData()
            ));
            $invoice->setDateOfPayment($invoiceGenerator->makeDateTimeFromString(
                $form->get('dateOfPayment')->getData()
            ));
            $invoice->setSaleDate($invoiceGenerator->makeDateTimeFromString(
                $form->get('saleDate')->getData()
            ));

            $contractorId = $form->get('contractor_id')->getData();
            $contractor = $em->getRepository(Contractor::class)
                ->find($contractorId);

            $invoiceGenerator->makeCustomer($contractor);
            $invoiceGenerator->setItems($form->get('items')->getData());
            $invoiceGenerator->generateItems();
            $invoiceGenerator->makeTotalNetPrice();
            $invoiceGenerator->makeTotalGrossPrice();

            $em->persist($invoice);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Pomyślnie dodano fakture o numerze ' . $invoice->getInvoiceNumber());

            return $this->redirectToRoute('invoice_index');
        }

        return $this->render('application/invoice/create.html.twig', array(
            'form' => $form->createView(),
            'customerName' => is_null($contractor) ? '' : $contractor->getName(),
            'customerNip' => is_null($contractor) ? '' : $contractor->getNip(),
            'customerId' => is_null($contractor) ? '' : $contractor->getId(),
        ));
    }

    /**
     * @Route("/aplikacja/faktury/wystaw-ponownie/{id}", name="invoice_again")
     *
     * @param Invoice $invoice
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createFromExistsAction(Invoice $invoice = null, Request $request){

        if ($invoice == null){
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Faktura nie istanije');

            return $this->redirectToRoute('invoice_index');
        }

        $user = $this->getUser();
        if ($user != $invoice->getUser()){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $em = $this->getDoctrine()->getManager();

        $newInvoice = new Invoice($invoice->getUser());
        $invoiceGenerator = $this->get('app.invoice_generator');
        $invoiceGenerator->setInvoice($newInvoice);
        $invoiceGenerator->addItems($invoice->getItems());
        $invoiceGenerator->generateNumber();
        $invoiceGenerator->makeCustomerFromExistInvoice($invoice->getCustomer());

        $form = $this->createForm(InvoiceType::class, $newInvoice);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()){

            $newInvoice->setDateOfIssue($invoiceGenerator->makeDateTimeFromString(
                $form->get('dateOfIssue')->getData()
            ));
            $newInvoice->setDateOfPayment($invoiceGenerator->makeDateTimeFromString(
                $form->get('dateOfPayment')->getData()
            ));
            $newInvoice->setSaleDate($invoiceGenerator->makeDateTimeFromString(
                $form->get('saleDate')->getData()
            ));

            $contractorId = $form->get('contractor_id')->getData();
            $contractor = $em->getRepository(Contractor::class)
                ->find($contractorId);

            if ($contractor != NULL)
                $invoiceGenerator->makeCustomer($contractor);

            $invoiceGenerator->setItems($form->get('items')->getData());
            $invoiceGenerator->generateItems();
            $invoiceGenerator->makeTotalNetPrice();
            $invoiceGenerator->makeTotalGrossPrice();

            $em->persist($newInvoice);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Pomyślnie dodano fakture o numerze ' . $newInvoice->getInvoiceNumber());

            return $this->redirectToRoute('invoice_index');
        }

        return $this->render('application/invoice/create.html.twig', array(
            'form' => $form->createView(),
            'customerName' => $invoice->getCustomer()->getName(),
            'customerNip' => $invoice->getCustomer()->getNip(),
            'customerId' => $invoice->getCustomer()->getId(),
        ));
    }

//    /**
//     * @Route("/aplikacja/faktury/edycja-faktury/{id}", name="invoice_update")
//     */
//    public function updateAction(Invoice $invoice){
//
//    }

    /**
     * @Route("/aplikacja/faktury/usun-fakture/{id}", name="invoice_delete")
     *
     * @param Request $request
     * @param Invoice $invoice
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Invoice $invoice = null){

        if ($invoice == null){
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Faktura nie istnieje');

            return $this->redirectToRoute('invoice_index');
        }

        $user = $this->getUser();

        if ($user != $invoice->getUser()){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $em = $this->getDoctrine()->getManager();
        $invoiceNumber = $invoice->getInvoiceNumber();
        $invoice->getUser()->removeInvoice($invoice);
        $invoice->setDelete(1);
        $em->flush();

        $request->getSession()
            ->getFlashBag()
            ->add('success', 'Pomyślnie usunięto fakture o numerze ' . $invoiceNumber);

        return $this->redirectToRoute('invoice_index');
    }

    /**
     * @Route("/aplikacja/faktury/znajdz", name="contractors_for_invoice_search")
     *
     * @param Request $request
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function searchContractorsAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return new JsonResponse(array(
                'status' => FALSE,
                'message' => 'Brak dostępu',
            ), 403);
        }

        $input = $request->request->get('input');
        $repo = $this->getDoctrine()->getRepository(Contractor::class);
        $contractors = $repo->searchContractors($user, $input);

        if ($contractors){
            return new JsonResponse(array(
                'status' => TRUE,
                'data' => json_encode($contractors),
            ));
        } else {
            return new JsonResponse(array(
                'status' => FALSE,
            ));
        }
    }

    /**
     * @Route("/aplikacja/faktury/znajdz-produkt", name="products_for_invoice_search")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchProductsAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return new JsonResponse(array(
                'status' => FALSE,
                'user' => $user
            ), 403);
        }

        $productName = $request->request->get('input');
        $repo = $this->getDoctrine()->getRepository(Product::class);
        $products = $repo->ajaxFindProducts($productName, $user);

        if ($products){
            return new JsonResponse(array(
                'status' => TRUE,
                'data' => json_encode($products),
                'producrs' => $products
            ), 200);
        } else {
            return new JsonResponse(array(
                'status' => TRUE,
                'producrs' => $products
            ), 200);
        }

    }

    /**
     * @Route("/aplikacja/faktury/dodaj-kontrahenta", name="create_contractor_from_invoice")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createCustomerAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return new JsonResponse(array(
                'status' => FALSE,
                'message' => 'Brak dostępu',
            ), 403);
        }

        $name = $request->request->get('name');
        $nip = $request->request->get('nip');
        $zipCode = $request->request->get('zipCode');
        $city = $request->request->get('city');
        $street = $request->request->get('street');

        $contractor = new Contractor();
        $contractor->setUser($user);
        $contractor->setName($name);
        $contractor->setNip(intval($nip));
        $contractor->setZipCode($zipCode);
        $contractor->setCity($city);
        $contractor->setStreet($street);

        $em = $this->getDoctrine()->getManager();
        $em->persist($contractor);
        $em->flush();

        return new JsonResponse(array(
            'name' => $contractor->getName(),
            'nip' => $contractor->getNip(),
            'id' => $contractor->getId(),
        ),200);
    }

    /**
     * @Route("/aplikacja/faktury/znajdz-fakture", name="invoice_search")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchInvoiceAction(Request $request){

        $result = array();

        if (!$this->getUser())
            return new JsonResponse($result, 200);

        $result = $this->getDoctrine()
            ->getRepository(Invoice::class)
            ->searchInvoice($this->getUser(), $request->request->get('input'));

        return new JsonResponse($result, 200);
    }

    /**
     * @Route("/aplikacja/faktury/zaplacono/{id}", name="invoice_paid")
     *
     * @param Request $request
     * @param Invoice $invoice
     *
     * @return JsonResponse|RedirectResponse
     */
    public function paidAction(Request $request, Invoice $invoice = null){

        if ($invoice == null){
            return new JsonResponse([
                'status' => FALSE,
                'message' => 'Faktura nie istnieje',
            ], 200);
        }

        $user = $this->getUser();
        if (!$user || ($user != $invoice->getUser())){
            return new JsonResponse([
                'status' => FALSE,
                'message' => 'Brak dostępu',
            ], 200);
        }

        $em = $this->getDoctrine()->getManager();
        $invoice->setPaid(1);
        $em->flush();

        $request->getSession()
            ->getFlashBag()
            ->add('success', 'Pomyślnie zaktualizowano fakture o numerze ' . $invoice->getInvoiceNumber());

        return $this->redirectToRoute('invoice_index');
    }

    /**
     * @Route("/aplikacja/faktury/wyslij", name="invoice_email")
     *
     * @param Request $request
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function sendInvoiceAction(Request $request){

        $user = $this->getUser();

        $invoice = $this->getDoctrine()
            ->getRepository(Invoice::class)
            ->find((int) $request->request->get('invoice_id'));

        if ($invoice == NULL)
            return $this->render('errors/403.html.twig', array(
                'message' => 'Faktura nie istanieje'
            ));

        if ($user != $invoice->getUser())
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));

        $fileName = md5(time());
        $path = __DIR__.'../../../../web/pdf/'.$fileName.'.pdf';
        $html = $this->renderView('application/invoice/invoicePDF.html.twig',[
            'invoice' => $invoice
        ]);
        $this->get('knp_snappy.pdf')->generateFromHtml($html, $path);

        if ($request->request->get('subject') != ''){
            $subject = $request->request->get('subject');
        } else{
            $subject = "Otrzymano fakturę - Internetowy system do wystawiania faktur";
        }

        $odbiorca = $request->get('odbiorca');
        if (empty($odbiorca) && null == $odbiorca){

            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Wystąpił błąd - nie udało sie wysłać faktury, musisz podać adres e-mail odbiorcy');

            return $this->redirectToRoute('invoice_index');
        }
        $body = '';
        if ($request->request->get('body') != ''){
            $body = $request->request->get('body').' ';
        }
        $body .= "\nW załączniku znajduje się faktura od " . $invoice->getUser()->getName();

        $mailer = new MailerService(
                    new PHPMailer(TRUE),
                    $this->container->getParameter('mailer_password')
                );

        $mailer->setFrom($invoice->getUser()->getEmail(), $invoice->getUser()->getName(), 'Internetowy system do fakturowania');
        $mailer->setAddress($odbiorca, $invoice->getCustomer()->getName());
        $mailer->setContent($subject, $body);
        $mailer->addAttachment($path, 'faktura.pdf');
        $result = $mailer->send();

        if ($result == TRUE)
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Pomyślnie wysłano fakture o numerze ' . $invoice->getInvoiceNumber());
        else
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Wystąpił błąd - nie udało sie wysłać faktury');

        return $this->redirectToRoute('invoice_index');
    }

    /**
     * @Route("/aplikacja/faktury/generuj-pdf/{id}", name="invoice_pdf")
     *
     * @param Invoice $invoice
     *
     * @return PdfResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function invoiceToPDFAction(Invoice $invoice = null){

        $user = $this->getUser();

        if ($invoice == NULL)
            return $this->render('errors/403.html.twig', array(
                'message' => 'Faktura nie istanieje'
            ));

        if ($user != $invoice->getUser())
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));

        $html = $this->renderView('application/invoice/invoicePDF.html.twig',[
            'invoice' => $invoice
        ]);

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            'faktura.pdf'
        );
    }

    /**
     * @Route("/aplikacja/faktury/contextmenu", name="invoice_contextmenu")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function contexmenuAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return new JsonResponse([
                'status' => FALSE,
                'message' => 'Brak dostępu'
            ], 201);
        }

        $target = $request->request->get('target');
        $invoiceId = $request->request->get('id');

        if ($target == 'generate-pdf'){
            $target = 0;
        } else if($target == 'delete'){
            $target = 1;
        } else if ($target == 'generate-again') {
            $target = 2;
        } else if ($target == 'set-paid'){
            $target = 3;
        } else if ($target == 'send-email'){
            $target = 4;
        } else{
            $target = -1;
        }

        return new JsonResponse([
            'status' => TRUE,
            'target' => $target,
            'data' => intval($invoiceId),
        ], 200);
    }

}
