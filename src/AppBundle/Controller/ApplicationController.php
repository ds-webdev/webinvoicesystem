<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contractor;
use AppBundle\Entity\Invoice;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class ApplicationController extends Controller {

    /**
     * @Route("/aplikacja", name="application")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(){

        $user = $this->getUser();

        if (!$user){
            return $this->render('home/index.html.twig');
        }


        $repo = $this->getDoctrine()->getRepository(User::class);
        $products = $repo->countProductsForUser($user);
        $contractors = $repo->countContractorsForUser($user);
        $invoices = $repo->countInvoiceForUser($user);

        return $this->render('application/index.html.twig', array(
            'products' => $products,
            'contractors' => $contractors,
            'invoices' => $invoices,
        ));
    }

    /**
     * @Route("/aplikacja/faktury", name="invoice_index")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function invoiceAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $repo = $this->getDoctrine()->getRepository(Invoice::class);
        $invoices = $repo->findBy(array(
            'user' => $user,
            'delete' => 0,
        ));
        $invoices = is_null($invoices) ? array() : $invoices;

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($invoices, $request->get('page', 1), 20);

        return $this->render('application/invoice/index.html.twig', array(
            'invoices' => $pagination
        ));
    }

    /**
     * @Route("/aplikacja/kontrahenci", name="contractor_index")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contractorAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $repo = $this->getDoctrine()->getRepository(Contractor::class);
        $contractors = $repo->findBy(array(
            'user' => $user
        ));

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(is_null($contractors) ? array() : $contractors, $request->get('page', 1), 20);

        return $this->render('application/contractor/index.html.twig', array(
            'contractors' => $pagination
        ));
    }

    /**
     * @Route("/aplikacja/produkty-i-uslugi", name="product_index")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function productAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $repo = $this->getDoctrine()->getRepository(Product::class);
        $products = $repo->findBy(['user' => $user]);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(empty($products) ? array() : $products, $request->get('page', 1), 20);

        return $this->render('application/product/index.html.twig', array(
            'products' => $pagination,
        ));
    }

}
