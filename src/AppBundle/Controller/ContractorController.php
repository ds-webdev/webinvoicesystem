<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contractor;
use AppBundle\Entity\User;
use AppBundle\Form\ContractorType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ContractorController extends Controller {

    /**
     * @Route("/aplikacja/kontrahenci/{id}", name="contractor_show")
     *
     * @param Contractor|null $contractor
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Contractor $contractor = null, Request $request){

        if ($contractor == null){
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Kontrahent nie istnieje');

            return $this->redirectToRoute('contractor_index');
        }

        $user = $this->getUser();
        if ($user != $contractor->getUser()){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        return $this->render('application/contractor/show.html.twig', array(
            'contractor' => $contractor
        ));
    }

    /**
     * @Route("/aplikacja/kontrahenci/dodaj", name="contractor_create")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $contractor = new Contractor();
        $contractor->setUser($user);

        $form = $this->createForm(ContractorType::class, $contractor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($contractor);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Pomyślnie dodano kontrahenta.');

            return $this->redirectToRoute('contractor_index');
        }

        return $this->render('application/contractor/create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/aplikacja/kontrahenci/edycja/{id}", name="contractor_update")
     *
     * @param Request $request
     * @param Contractor $contractor
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, Contractor $contractor){

        $user = $this->getUser();

        if ($user != $contractor->getUser()){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $form = $this->createForm(ContractorType::class, $contractor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($contractor);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Edycja zakończona powodzeniem.');

            return $this->redirectToRoute('contractor_index');
        }

        return $this->render('application/contractor/update.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/aplikacja/kontrahenci/usun/{id}", name="contractor_delete")
     *
     * @param Request $request
     * @param Contractor $contractor
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, Contractor $contractor){

        $user = $this->getUser();

        if ($user != $contractor->getUser()){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $contractorName = $contractor->getName();
        $em = $this->getDoctrine()->getManager();
        $contractor->getUser()->removeContractor($contractor);
        $em->remove($contractor);
        $em->flush();

        $request->getSession()
            ->getFlashBag()
            ->add('success', 'Usuwanie zakończone powodzeniem - ' . $contractorName);

        return $this->redirectToRoute('contractor_index');
    }

    /**
     * @Route("/aplikacja/kontrahenci/znajdz", name="contractor_ajax_search")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchContractorsAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $expression = $request->request->get('input');

        $repo = $this->getDoctrine()->getRepository(Contractor::class);
        $result = $repo->searchContractors($user, $expression);

        if (empty($result)){
            return new JsonResponse(array(
                'status' => FALSE,
            ));
        } else {
            return new JsonResponse(array(
                'status' => TRUE,
                'data' => json_encode($result),
            ));
        }
    }

    /**
     * @Route("/aplikacja/kontrahenci/contextmenu", name="contractor_contextmenu")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contextmenuAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $id = $request->request->get('id');
        $target = $request->request->get('target');
        if ($target == 'edit'){
            $target = 1;
        } else if($target == 'delete'){
            $target = 2;
        } else if ($target == 'invoice_create') {
            $target = 3;
        } else {
            $target = 0;
        }
        $repo = $this->getDoctrine()->getRepository(Contractor::class);
        $contractor = $repo->find(intval($id));

        if (empty($contractor)){
            return new JsonResponse(array(
                'status' => FALSE,
            ));
        } else {
            return new JsonResponse(array(
                'status' => TRUE,
                'target' => $target,
                'data' => json_encode($contractor->getId()),
            ));
        }
    }

}
