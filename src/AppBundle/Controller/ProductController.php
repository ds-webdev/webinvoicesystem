<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use Doctrine\DBAL\Exception\DriverException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller {

    /**
     * @Route("/aplikacja/kontrahenci/{id}", name="contractor_show")
     *
     * @param Product|null $product
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Product $product = null, Request $request){

        if ($product == null){
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Produkt nie istnieje');

            return $this->redirectToRoute('product_index');
        }

        $user = $this->getUser();
        if ($user != $product->getUser()){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        return $this->render('application/product/show.html.twig', array(
            'product' => $product
        ));
    }

    /**
     * @Route("/aplikacja/produkty-i-uslugi/dodaj", name="product_create")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $product = new Product();
        $product->setUser($user);

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Dodawanie zakończyło się poprawnie.');

            return $this->redirectToRoute('product_index');
        }

        return $this->render('application/product/create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/aplikacja/produkty-i-uslugi/edycja/{id}", name="product_update")
     *
     * @param Request $request
     * @param Product $product
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, Product $product){

        $user = $this->getUser();

        if ($user != $product->getUser()){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Edycja zakończyła się poprawnie');

            return $this->redirectToRoute('product_index');
        }

        return $this->render('/application/product/update.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/aplikacja/produkty-i-uslugi/usun/{id}", name="product_delete")
     *
     * @param Request $request
     * @param Product $product
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, Product $product){

        $user = $this->getUser();

        if ($user != $product->getUser()){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $productName = $product->getName();
        $em = $this->getDoctrine()->getManager();
        $product->getUser()->removeProduct($product);
        $em->remove($product);
        $em->flush();

        $request->getSession()
            ->getFlashBag()
            ->add('success', 'Pomyślnie usunięto produkt\usługę - ' . $productName);

        return $this->redirectToRoute('product_index');
    }

    /**
     * @Route("/aplikacja/produkty-i-uslugi/znajdz", name="products_ajax_search")
     *
     * @param Request $request
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function searchProductsAction(Request $request){

        $user = $this->getUser();

        if (!$user){
            return $this->render('errors/403.html.twig', array(
                'message' => 'Brak dostępu'
            ));
        }

        $input = $request->request->get('input');
        $repo = $this->getDoctrine()->getRepository(Product::class);
        $products = $repo->ajaxFindProducts($input, $user);

        if (empty($products)){
            return new JsonResponse(array(
                'status' => FALSE,
            ));
        } else {
            return new JsonResponse(array(
                'status' => TRUE,
                'data' => json_encode($products),
            ));
        }
    }

}
