var gulp = require('gulp');
var gulpif = require('gulp-if');
var uglifycss = require('gulp-uglifycss');
var uglify      = require('gulp-uglifyjs');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var livereload = require('gulp-livereload');
var autoprefixer = require('gulp-autoprefixer');
var env = process.env.GULP_ENV;
livereload({ start: true });
//JAVASCRIPT TASK: write one minified js file out of jquery.js, bootstrap.js and all of my custom js files
gulp.task('js', function () {
    return gulp.src([
        'bower_components/jquery/dist/jquery.js',
        'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
        'app/Resources/public/js/**/*.js'])
        .pipe(concat('javascript.js'))
        .pipe(uglify())
        .pipe(gulp.dest('web/js'));
});

//CSS TASK: write one minified css file out of bootstrap.less and all of my custom less files
gulp.task('sass', function () {
    return gulp.src([
        'app/Resources/public/sass/main.*'])
        .pipe(gulpif(/[.]scss/, sass()))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(concat('styles.css'))
        .pipe(uglifycss())
        .pipe(gulp.dest('web/css'))
        .pipe(livereload());
});

//IMAGE TASK: Just pipe images from project folder to public web folder
gulp.task('img', function() {
    return gulp.src('app/Resources/public/img/**/*.*')
        .pipe(gulp.dest('web/img'));
});

//define executable tasks when running "gulp" command
gulp.task('default', ['js', 'sass', 'img']);

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('app/Resources/public/sass/**/*.scss', ['sass']);
    gulp.watch('app/Resources/public/js/**/*.js', ['js']);
    gulp.watch('app/Resources/public/img/**/*.*', ['img']);
});